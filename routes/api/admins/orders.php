<?php

use App\Http\Controllers\Api\Admins\OrderController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:admin']], function () {
    Route::post('/approve', [OrderController::class, 'approve']);
    Route::post('/prepare', [OrderController::class, 'prepare']);
    Route::post('/getArchived', [OrderController::class, 'getArchived']);
    Route::post('/getPending', [OrderController::class, 'getPending']);
    Route::post('/getAccepted', [OrderController::class, 'getAccepted']);
    Route::post('/getDetails', [OrderController::class, 'getDetails']);
});
