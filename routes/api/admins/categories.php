<?php

use App\Http\Controllers\Api\Admins\CategoryController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:admin']], function () {
    Route::post('/create', [CategoryController::class, 'create']);
    Route::post('/get', [CategoryController::class, 'get']);
    Route::post('/delete', [CategoryController::class, 'delete']);
    Route::post('/update', [CategoryController::class, 'update']);
});