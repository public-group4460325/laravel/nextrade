<?php

use App\Http\Controllers\Api\Admins\ItemController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:admin']], function () {
    Route::post('/create', [ItemController::class, 'create']);
    Route::post('/delete', [ItemController::class, 'delete']);
    Route::post('/update', [ItemController::class, 'update']);
    Route::post('/get', [ItemController::class, 'get']);
    Route::post('/search', [ItemController::class, 'search']);
});