<?php

use App\Http\Controllers\Api\Deliveries\OrderController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:delivery']], function () {
    Route::post('/approve', [OrderController::class, 'approve']);
    Route::post('/deliver', [OrderController::class, 'deliver']);
    Route::post('/getPending', [OrderController::class, 'getPending']);
    Route::post('/getArchived', [OrderController::class, 'getArchived']);
    Route::post('/getAccepted', [OrderController::class, 'getAccepted']);
    Route::post('/getDetails', [OrderController::class, 'getDetails']);
});
