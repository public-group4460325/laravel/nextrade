<?php

use App\Http\Controllers\Api\Users\CouponController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:user']], function () {
    Route::post('/check', [CouponController::class, 'check']);
});
