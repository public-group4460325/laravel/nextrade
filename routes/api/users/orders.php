<?php

use App\Http\Controllers\Api\Users\OrderController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:user']], function () {
    Route::post('/add', [OrderController::class, 'add']);
    Route::post('/getPending', [OrderController::class, 'getPending']);
    Route::post('/getArchived', [OrderController::class, 'getArchived']);
    Route::post('/getDetails', [OrderController::class, 'getDetails']);
    Route::post('/delete', [OrderController::class, 'delete']);
    Route::post('/rate', [OrderController::class, 'rate']);
});
