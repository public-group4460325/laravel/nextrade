<?php

use App\Http\Controllers\Api\Users\CategoryController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:user']], function () {
    Route::post('/get', [CategoryController::class, 'get']);
    Route::post('/getHomeData', [CategoryController::class, 'getHomeData']);
});