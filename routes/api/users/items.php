<?php

use App\Http\Controllers\Api\Users\ItemController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:user']], function () {
    Route::post('/getItems', [ItemController::class, 'get']);
    Route::post('/search', [ItemController::class, 'search']);
});