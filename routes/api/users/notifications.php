<?php

use App\Http\Controllers\Api\Users\NotificationController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:user']], function () {
    Route::post('/get', [NotificationController::class, 'get']);
});
