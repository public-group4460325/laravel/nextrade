<?php

use App\Http\Controllers\Api\Users\FavoriteController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:user']], function () {
    Route::post('/addOrRemove', [FavoriteController::class, 'addOrRemove']);
    Route::post('/get', [FavoriteController::class, 'get']);
});
