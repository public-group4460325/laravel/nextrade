<?php

use App\Http\Controllers\Api\Users\AddressController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['auth:sanctum', 'ability:user']], function () {
    Route::post('/get', [AddressController::class, 'get']);
    Route::post('/create', [AddressController::class, 'create']);
    Route::post('/update', [AddressController::class, 'update']);
    Route::post('/delete', [AddressController::class, 'delete']);
});
