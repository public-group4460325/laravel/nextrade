<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class MakeLogObservers extends Command
{
    protected $signature = 'make:log-observers';
    protected $description = 'Create a new Observer with Logging for the specified model';

    public function handle()
    {
        if (!File::exists(app_path('Observers'))) {
            File::makeDirectory(app_path('Observers'));
        }
        
        $path = app_path('Models');
        $files = glob($path . '/*.php'); 

        foreach ($files as $file) {
            $model = pathinfo($file, PATHINFO_FILENAME);
            $observerClass = $model . 'Observer';
            $observerPath = app_path("Observers/{$observerClass}.php");
            
            $stub = File::get(resource_path('stubs/log-observer.stub'));
            $stub = str_replace('{{ model }}', $model, $stub);
            $stub = str_replace('{{ modelVariable }}', Str::camel($model), $stub);

            File::put($observerPath, $stub);
        }

        $this->info("Observer created for {$model} at {$observerPath}");
    }
}
