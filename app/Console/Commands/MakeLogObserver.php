<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class MakeLogObserver extends Command
{
    protected $signature = 'make:log-observer {model}';
    protected $description = 'Create a new Observer with Logging for the specified model';

    public function handle()
    {
        $model = $this->argument('model');
        $observerClass = $model . 'Observer';
        $observerPath = app_path("Observers/{$observerClass}.php");

        if (!File::exists(app_path('Observers'))) {
            File::makeDirectory(app_path('Observers'));
        }

        $stub = File::get(resource_path('stubs/log-observer.stub'));
        $stub = str_replace('{{ model }}', $model, $stub);
        $stub = str_replace('{{ modelVariable }}', Str::camel($model), $stub);

        File::put($observerPath, $stub);

        $this->info("Observer created for {$model} at {$observerPath}");
    }
}
