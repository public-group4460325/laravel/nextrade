<?php

namespace App\Repositories\Api\Auth;

use App\Models\Login;
use App\Models\User;
use App\Models\UserType;

class LoginRepository
{
    public function getUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    public function saveUser($user)
    {
        $user->save();
    }

    public function getUserType($user_type_id)
    {
        return UserType::find($user_type_id)->type;
    }
}
