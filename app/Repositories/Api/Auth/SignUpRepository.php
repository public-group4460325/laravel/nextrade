<?php

namespace App\Repositories\Api\Auth;

use App\Models\SignUp;
use App\Models\User;

class SignUpRepository
{
    public function getUserByEmail($email)
    {
        return User::where('email', $email)->first();
    }

    public function createUser($data)
    {
        User::create($data);
    }

    public function saveUser($user)
    {
        $user->save();
    }
}
