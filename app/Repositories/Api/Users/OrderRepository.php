<?php

namespace App\Repositories;

use App\Models\Address;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Rate;

class OrderRepository
{
    public function getPendingOrders($userId)
    {
        return Order::where('user_id', $userId)->whereNot('status', 4)->get();
    }

    public function getArchivedOrders($userId)
    {
        return Order::where('user_id', $userId)->where('status', 4)->get();
    }

    public function findOrder($orderId)
    {
        return Order::find($orderId);
    }

    public function createOrder($data)
    {
        return Order::create($data);
    }

    public function updateCoupon($coupon)
    {
        $coupon->count--;
        $coupon->save();
    }

    public function getItemsByOrder($orderId)
    {
        return Cart::where('order_id', $orderId)->get();
    }

    public function findAddress($userId, $addressId)
    {
        return Address::where('user_id', $userId)->find($addressId);
    }

    public function findCoupon($couponId)
    {
        return Coupon::find($couponId);
    }

    public function findRate($userId, $orderId)
    {
        return Rate::where('user_id', $userId)->where('order_id', $orderId)->first();
    }

    public function deleteOrder($order)
    {
        return $order->delete();
    }

    public function createRate($data)
    {
        return Rate::create($data);
    }
}
