<?php

namespace App\Repositories;

use App\Models\Cart;
use App\Models\Item;

class CartRepository
{
    public function findItem($itemId)
    {
        return Item::find($itemId);
    }

    public function findCartItem($userId, $itemId)
    {
        return Cart::where('user_id', $userId)->where('item_id', $itemId)->whereNull('order_id')->first();
    }

    public function create(array $data)
    {
        return Cart::create($data);
    }

    public function update(Cart $cart, array $data)
    {
        $cart->update($data);
    }

    public function delete(Cart $cart)
    {
        $cart->delete();
    }

    public function getCartItems($userId)
    {
        return Cart::where('user_id', $userId)->whereNull('order_id')->get();
    }

    public function getTopItems()
    {
        return Cart::whereNotNull('order_id')
            ->groupBy('item_id')
            ->selectRaw('item_id, sum(count) as count')
            ->orderBy('count', 'desc')
            ->take(10)
            ->get();
    }
}
