<?php

namespace App\Repositories;

use App\Models\Address;

class AddressRepository
{
    public function getByUserId($userId)
    {
        return Address::where('user_id', $userId)->get();
    }

    public function find($addressId)
    {
        return Address::find($addressId);
    }

    public function create(array $data)
    {
        return Address::create($data);
    }

    public function update(Address $address, array $data)
    {
        $address->update($data);
    }

    public function delete(Address $address)
    {
        $address->delete();
    }
}
