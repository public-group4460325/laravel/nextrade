<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Item;

class ItemRepository
{
    public function getOffers()
    {
        return Item::whereNot('discount', 0)
            ->where('active', true)
            ->with('category')
            ->get();
    }

    public function getItemsByCategory($categoryId)
    {
        $category = Category::find($categoryId);
        if (!$category) {
            return null;
        }
        return Item::where('category_id', $categoryId)->get();
    }

    public function searchItems($name)
    {
        return Item::where('name_en', 'LIKE', "%$name%")
                    ->orWhere('name_ar', 'LIKE', "%$name%")
                    ->get();
    }
}
