<?php

namespace App\Repositories;

use App\Models\Notification;

class NotificationRepository
{
    public function getNotificationsByUserId($userId)
    {
        return Notification::where('user_id', $userId)
                            ->orderBy('id', 'desc')
                            ->get();
    }
}
