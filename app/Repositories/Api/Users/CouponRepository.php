<?php

namespace App\Repositories;

use App\Models\Coupon;

class CouponRepository
{
    public function findByCode($code)
    {
        return Coupon::firstWhere('code', $code);
    }
}
