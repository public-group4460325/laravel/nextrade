<?php

namespace App\Repositories;

use App\Models\Favorite;

class FavoriteRepository
{
    public function findByUserIdAndItemId($userId, $itemId)
    {
        return Favorite::where('user_id', $userId)->where('item_id', $itemId)->first();
    }

    public function create($data)
    {
        return Favorite::create($data);
    }

    public function delete($favorite)
    {
        return $favorite->delete();
    }
}
