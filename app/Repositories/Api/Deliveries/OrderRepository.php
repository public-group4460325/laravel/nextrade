<?php

namespace App\Repositories;

use App\Models\Order;

class OrderRepository
{
    public function find($id)
    {
        return Order::find($id);
    }

    public function save(Order $order)
    {
        $order->save();
    }

    public function findByStatus($status)
    {
        return Order::where('status', $status)->get();
    }

    public function findByStatusAndDeliveryId($status, $deliveryId)
    {
        return Order::where('status', $status)->where('delivery_id', $deliveryId)->get();
    }
}
