<?php

namespace App\Repositories;

use App\Models\Address;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Order;

class OrderRepository
{
    public function getArchived()
    {
        return Order::where('status', 4)->get();
    }

    public function getPending()
    {
        return Order::where('status', 0)->get();
    }

    public function getAccepted()
    {
        return Order::whereNot('status', 0)->whereNot('status', 4)->get();
    }

    public function find($id)
    {
        return Order::find($id);
    }

    public function updateStatus($order, $status)
    {
        $order->status = $status;
        $order->save();
    }

    public function getItems($orderId)
    {
        return Cart::where('order_id', $orderId)->get();
    }

    public function getAddress($addressId)
    {
        return Address::find($addressId);
    }

    public function findCoupon($couponId)
    {
        return Coupon::find($couponId);
    }
}
