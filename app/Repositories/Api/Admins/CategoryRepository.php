<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Support\Facades\Storage;

class CategoryRepository
{
    public function create(array $data)
    {
        return Category::create($data);
    }

    public function find($id)
    {
        return Category::find($id);
    }

    public function delete(Category $category)
    {
        Storage::disk('public')->delete($category->image);
        $category->delete();
    }

    public function update(Category $category, array $data)
    {
        if (isset($data['image'])) {
            Storage::disk('public')->delete($category->image);
            $category->image = $data['image'];
        }

        $category->name_en = $data['name_en'];
        $category->name_ar = $data['name_ar'];

        $category->save();
    }

    public function getAll()
    {
        return Category::all();
    }
}
