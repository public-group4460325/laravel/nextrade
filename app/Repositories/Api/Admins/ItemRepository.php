<?php

namespace App\Repositories;

use App\Models\Item;
use Illuminate\Support\Facades\Storage;

class ItemRepository
{
    public function create(array $data)
    {
        return Item::create($data);
    }

    public function find($id)
    {
        return Item::find($id);
    }

    public function delete($item)
    {
        Storage::disk('public')->delete($item->image);
        $item->delete();
    }

    public function update($item, array $data)
    {
        if (isset($data['image'])) {
            Storage::disk('public')->delete($item->image);
            $item->image = $data['image']->store('images/items', 'public');
        }
        $item->update($data);
    }

    public function getByCategory($categoryId)
    {
        return Item::where('category_id', $categoryId)->get();
    }

    public function search($name)
    {
        return Item::where('name_en', 'LIKE', "%$name%")
            ->orWhere('name_ar', 'LIKE', "%$name%")
            ->get();
    }
}
