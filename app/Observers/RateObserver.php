<?php

namespace App\Observers;

use App\Models\Rate;
use Illuminate\Support\Facades\Log;

class RateObserver
{
    /**
     * Handle the Rate "created" event.
     */
    public function created(Rate $rate): void
    {
        Log::info('Rate Created: ', $rate->toArray());
    }

    /**
     * Handle the Rate "updated" event.
     */
    public function updated(Rate $rate): void
    {
        Log::info('Rate Updated: ', $rate->toArray());
    }

    /**
     * Handle the Rate "deleted" event.
     */
    public function deleted(Rate $rate): void
    {
        Log::info('Rate Deleted: ', $rate->toArray());
    }

    /**
     * Handle the Rate "restored" event.
     */
    public function restored(Rate $rate): void
    {
        Log::info('Rate Restored: ', $rate->toArray());
    }

    /**
     * Handle the Rate "force deleted" event.
     */
    public function forceDeleted(Rate $rate): void
    {
        Log::info('Rate ForceDeleted: ', $rate->toArray());
    }
}
