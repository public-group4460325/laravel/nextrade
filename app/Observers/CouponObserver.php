<?php

namespace App\Observers;

use App\Models\Coupon;
use Illuminate\Support\Facades\Log;

class CouponObserver
{
    /**
     * Handle the Coupon "created" event.
     */
    public function created(Coupon $coupon): void
    {
        Log::info('Coupon Created: ', $coupon->toArray());
    }

    /**
     * Handle the Coupon "updated" event.
     */
    public function updated(Coupon $coupon): void
    {
        Log::info('Coupon Updated: ', $coupon->toArray());
    }

    /**
     * Handle the Coupon "deleted" event.
     */
    public function deleted(Coupon $coupon): void
    {
        Log::info('Coupon Deleted: ', $coupon->toArray());
    }

    /**
     * Handle the Coupon "restored" event.
     */
    public function restored(Coupon $coupon): void
    {
        Log::info('Coupon Restored: ', $coupon->toArray());
    }

    /**
     * Handle the Coupon "force deleted" event.
     */
    public function forceDeleted(Coupon $coupon): void
    {
        Log::info('Coupon ForceDeleted: ', $coupon->toArray());
    }
}
