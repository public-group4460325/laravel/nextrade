<?php

namespace App\Observers;

use App\Models\Item;
use Illuminate\Support\Facades\Log;

class ItemObserver
{
    /**
     * Handle the Item "created" event.
     */
    public function created(Item $item): void
    {
        Log::info('Item Created: ', $item->toArray());
    }

    /**
     * Handle the Item "updated" event.
     */
    public function updated(Item $item): void
    {
        Log::info('Item Updated: ', $item->toArray());
    }

    /**
     * Handle the Item "deleted" event.
     */
    public function deleted(Item $item): void
    {
        Log::info('Item Deleted: ', $item->toArray());
    }

    /**
     * Handle the Item "restored" event.
     */
    public function restored(Item $item): void
    {
        Log::info('Item Restored: ', $item->toArray());
    }

    /**
     * Handle the Item "force deleted" event.
     */
    public function forceDeleted(Item $item): void
    {
        Log::info('Item ForceDeleted: ', $item->toArray());
    }
}
