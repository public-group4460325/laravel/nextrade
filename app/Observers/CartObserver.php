<?php

namespace App\Observers;

use App\Models\Cart;
use Illuminate\Support\Facades\Log;

class CartObserver
{
    /**
     * Handle the Cart "created" event.
     */
    public function created(Cart $cart): void
    {
        Log::info('Cart Created: ', $cart->toArray());
    }

    /**
     * Handle the Cart "updated" event.
     */
    public function updated(Cart $cart): void
    {
        Log::info('Cart Updated: ', $cart->toArray());
    }

    /**
     * Handle the Cart "deleted" event.
     */
    public function deleted(Cart $cart): void
    {
        Log::info('Cart Deleted: ', $cart->toArray());
    }

    /**
     * Handle the Cart "restored" event.
     */
    public function restored(Cart $cart): void
    {
        Log::info('Cart Restored: ', $cart->toArray());
    }

    /**
     * Handle the Cart "force deleted" event.
     */
    public function forceDeleted(Cart $cart): void
    {
        Log::info('Cart ForceDeleted: ', $cart->toArray());
    }
}
