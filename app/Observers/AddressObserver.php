<?php

namespace App\Observers;

use App\Models\Address;
use Illuminate\Support\Facades\Log;

class AddressObserver
{
    /**
     * Handle the Address "created" event.
     */
    public function created(Address $address): void
    {
        Log::info('Address Created: ', $address->toArray());
    }

    /**
     * Handle the Address "updated" event.
     */
    public function updated(Address $address): void
    {
        Log::info('Address Updated: ', $address->toArray());
    }

    /**
     * Handle the Address "deleted" event.
     */
    public function deleted(Address $address): void
    {
        Log::info('Address Deleted: ', $address->toArray());
    }

    /**
     * Handle the Address "restored" event.
     */
    public function restored(Address $address): void
    {
        Log::info('Address Restored: ', $address->toArray());
    }

    /**
     * Handle the Address "force deleted" event.
     */
    public function forceDeleted(Address $address): void
    {
        Log::info('Address ForceDeleted: ', $address->toArray());
    }
}
