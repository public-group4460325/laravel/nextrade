<?php

namespace App\Observers;

use App\Models\Favorite;
use Illuminate\Support\Facades\Log;

class FavoriteObserver
{
    /**
     * Handle the Favorite "created" event.
     */
    public function created(Favorite $favorite): void
    {
        Log::info('Favorite Created: ', $favorite->toArray());
    }

    /**
     * Handle the Favorite "updated" event.
     */
    public function updated(Favorite $favorite): void
    {
        Log::info('Favorite Updated: ', $favorite->toArray());
    }

    /**
     * Handle the Favorite "deleted" event.
     */
    public function deleted(Favorite $favorite): void
    {
        Log::info('Favorite Deleted: ', $favorite->toArray());
    }

    /**
     * Handle the Favorite "restored" event.
     */
    public function restored(Favorite $favorite): void
    {
        Log::info('Favorite Restored: ', $favorite->toArray());
    }

    /**
     * Handle the Favorite "force deleted" event.
     */
    public function forceDeleted(Favorite $favorite): void
    {
        Log::info('Favorite ForceDeleted: ', $favorite->toArray());
    }
}
