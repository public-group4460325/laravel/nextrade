<?php

namespace App\Observers;

use App\Models\UserType;
use Illuminate\Support\Facades\Log;

class UserTypeObserver
{
    /**
     * Handle the UserType "created" event.
     */
    public function created(UserType $userType): void
    {
        Log::info('UserType Created: ', $userType->toArray());
    }

    /**
     * Handle the UserType "updated" event.
     */
    public function updated(UserType $userType): void
    {
        Log::info('UserType Updated: ', $userType->toArray());
    }

    /**
     * Handle the UserType "deleted" event.
     */
    public function deleted(UserType $userType): void
    {
        Log::info('UserType Deleted: ', $userType->toArray());
    }

    /**
     * Handle the UserType "restored" event.
     */
    public function restored(UserType $userType): void
    {
        Log::info('UserType Restored: ', $userType->toArray());
    }

    /**
     * Handle the UserType "force deleted" event.
     */
    public function forceDeleted(UserType $userType): void
    {
        Log::info('UserType ForceDeleted: ', $userType->toArray());
    }
}
