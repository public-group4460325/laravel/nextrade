<?php

namespace App\Observers;

use App\Models\Notification;
use Illuminate\Support\Facades\Log;

class NotificationObserver
{
    /**
     * Handle the Notification "created" event.
     */
    public function created(Notification $notification): void
    {
        Log::info('Notification Created: ', $notification->toArray());
    }

    /**
     * Handle the Notification "updated" event.
     */
    public function updated(Notification $notification): void
    {
        Log::info('Notification Updated: ', $notification->toArray());
    }

    /**
     * Handle the Notification "deleted" event.
     */
    public function deleted(Notification $notification): void
    {
        Log::info('Notification Deleted: ', $notification->toArray());
    }

    /**
     * Handle the Notification "restored" event.
     */
    public function restored(Notification $notification): void
    {
        Log::info('Notification Restored: ', $notification->toArray());
    }

    /**
     * Handle the Notification "force deleted" event.
     */
    public function forceDeleted(Notification $notification): void
    {
        Log::info('Notification ForceDeleted: ', $notification->toArray());
    }
}
