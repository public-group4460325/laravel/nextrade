<?php

namespace App\Observers;

use App\Models\Setting;
use Illuminate\Support\Facades\Log;

class SettingObserver
{
    /**
     * Handle the Setting "created" event.
     */
    public function created(Setting $setting): void
    {
        Log::info('Setting Created: ', $setting->toArray());
    }

    /**
     * Handle the Setting "updated" event.
     */
    public function updated(Setting $setting): void
    {
        Log::info('Setting Updated: ', $setting->toArray());
    }

    /**
     * Handle the Setting "deleted" event.
     */
    public function deleted(Setting $setting): void
    {
        Log::info('Setting Deleted: ', $setting->toArray());
    }

    /**
     * Handle the Setting "restored" event.
     */
    public function restored(Setting $setting): void
    {
        Log::info('Setting Restored: ', $setting->toArray());
    }

    /**
     * Handle the Setting "force deleted" event.
     */
    public function forceDeleted(Setting $setting): void
    {
        Log::info('Setting ForceDeleted: ', $setting->toArray());
    }
}
