<?php

namespace App\Http\Requests\Api\Admins\Item;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->user()->tokenCan('admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'item_id' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required',
            'desk_ar' => 'required',
            'desk_en' => 'required',
            'count' => 'required',
            'active' => 'required',
            'price' => 'required',
            'discount' => 'required',
            'category_id' => 'required',
        ];
    }
}
