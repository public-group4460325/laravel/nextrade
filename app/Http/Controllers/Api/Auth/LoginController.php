<?php

namespace App\Http\Controllers\Api\Auth;

use App\Services\Api\Auth\LoginService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\CheckEmailRequest;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\ResetPasswordRequest;
use App\Http\Requests\Api\Auth\VerifyCodeForgetPasswordRequest;

class LoginController extends Controller
{
    protected $loginService;

    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }

    public function login(LoginRequest $request)
    {
        $validated = $request->validated();
        $response = $this->loginService->login($validated);
        return $this->returnResponse($response);
    }

    public function logout()
    {
        $response = $this->loginService->logout();
        return $this->returnResponse($response);
    }

    public function checkEmail(CheckEmailRequest $request)
    {
        $validated = $request->validated();
        $response = $this->loginService->checkEmail($validated);
        return $this->returnResponse($response);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $validated = $request->validated();
        $response = $this->loginService->resetPassword($validated);
        return $this->returnResponse($response);
    }

    public function verifyCodeForgetPassword(VerifyCodeForgetPasswordRequest $request)
    {
        $validated = $request->validated();
        $response = $this->loginService->verifyCodeForgetPassword($validated);
        return $this->returnResponse($response);
    }

    private function returnResponse($response)
    {
        if ($response['status'] === 'error') {
            return $this->returnError($response['message']);
        }
        return $this->returnData('data', $response['data']);
    }
}
