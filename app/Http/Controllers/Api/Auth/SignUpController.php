<?php

namespace App\Http\Controllers\Api\Auth;

use App\Services\Api\Auth\SignUpService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\ResendVerificationCodeRequest;
use App\Http\Requests\Api\Auth\SignUpRequest;
use App\Http\Requests\Api\Auth\VerifyCodeSignUpRequest;

class SignUpController extends Controller
{
    protected $signUpService;

    public function __construct(SignUpService $signUpService)
    {
        $this->signUpService = $signUpService;
    }

    public function signUp(SignUpRequest $request)
    {
        $validated = $request->validated();
        $response = $this->signUpService->signUp($validated);
        return $this->returnResponse($response);
    }

    public function verifyCodeSignUp(VerifyCodeSignUpRequest $request)
    {
        $validated = $request->validated();
        $response = $this->signUpService->verifyCodeSignUp($validated);
        return $this->returnResponse($response);
    }

    public function resendVerificationCode(ResendVerificationCodeRequest $request)
    {
        $validated = $request->validated();
        $response = $this->signUpService->resendVerificationCode($validated);
        return $this->returnResponse($response);
    }

    private function returnResponse($response)
    {
        if ($response['status'] === 'error') {
            return $this->returnError($response['message']);
        }
        return $this->returnSuccessMessage($response['message']);
    }
}
