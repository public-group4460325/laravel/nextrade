<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Item\GetRequest;
use App\Http\Requests\Api\Users\Item\SearchRequest;
use App\Services\ItemService;
use App\Traits\ApiTrait;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    use ApiTrait;

    protected $itemService;

    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    public function get(GetRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $data = $this->itemService->getItems($validated->category_id, $user);

        if ($data === null) {
            return $this->returnError('Category Not Found');
        }

        return $this->returnData('data', $data);
    }

    public function search(SearchRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $data = $this->itemService->searchItems($validated->name, $user);

        return $this->returnData('data', $data);
    }
}
