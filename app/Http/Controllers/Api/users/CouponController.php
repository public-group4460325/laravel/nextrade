<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Coupon\CheckRequest;
use App\Services\CouponService;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    protected $couponService;

    public function __construct(CouponService $couponService)
    {
        $this->couponService = $couponService;
    }

    public function check(CheckRequest $request)
    {
        $validated = $request->validated();
        $result = $this->couponService->checkCoupon($validated->code);

        if (isset($result['error'])) {
            return $this->returnError($result['error']);
        }

        return $this->returnData('data', $result['data']);
    }
}
