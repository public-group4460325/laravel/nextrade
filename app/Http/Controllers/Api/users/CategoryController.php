<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Traits\ApiTrait;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ApiTrait;

    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function get()
    {
        $data = $this->categoryService->getCategories();
        return $this->returnData('data', $data);
    }

    public function getHomeData()
    {
        $data = $this->categoryService->getHomeData();
        return $this->returnData('data', $data);
    }
}
