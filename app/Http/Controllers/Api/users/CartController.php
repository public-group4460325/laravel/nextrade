<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Cart\AddRequest;
use App\Http\Requests\Api\Users\Cart\DeleteRequest;
use App\Http\Requests\Api\Users\Cart\RemoveRequest;
use App\Http\Requests\Api\Users\Cart\UpdateRequest;
use App\Services\CartService;
use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $cartService;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function add(AddRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $message = $this->cartService->addItem($user->id, $validated->item_id, $validated->count);
        return $message === 'Added To Cart' ? $this->returnSuccessMessage($message) : $this->returnError($message);
    }

    public function remove(RemoveRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $message = $this->cartService->removeItem($user->id, $validated->item_id, $validated->count);
        return $message === 'Removed From Cart' ? $this->returnSuccessMessage($message) : $this->returnError($message);
    }

    public function get()
    {
        $user = auth()->user();
        $data = $this->cartService->getCartItems($user->id);
        return $this->returnData('data', $data);
    }

    public function update(UpdateRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $message = $this->cartService->updateItem($user->id, $validated->item_id, $validated->count);
        return $message === 'Updated' ? $this->returnSuccessMessage($message) : $this->returnError($message);
    }

    public function delete(DeleteRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $message = $this->cartService->deleteItem($user->id, $validated->item_id);
        return $message === 'Deleted' ? $this->returnSuccessMessage($message) : $this->returnError($message);
    }
}