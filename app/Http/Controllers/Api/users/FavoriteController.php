<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Favorite\AddOrRemoveRequest;
use App\Services\FavoriteService;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{
    protected $favoriteService;

    public function __construct(FavoriteService $favoriteService)
    {
        $this->favoriteService = $favoriteService;
    }

    public function addOrRemove(AddOrRemoveRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $result = $this->favoriteService->addOrRemove($user->id, $validated->item_id);

        if (isset($result['error'])) {
            return $this->returnError($result['error']);
        }

        return $this->returnSuccessMessage($result['success']);
    }

    public function get()
    {
        $user = auth()->user();
        $data = $this->favoriteService->getFavorites($user->id);
        return $this->returnData('data', $data);
    }
}
