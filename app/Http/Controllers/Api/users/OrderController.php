<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Order\AddRequest;
use App\Http\Requests\Api\Users\Order\DeleteRequest;
use App\Http\Requests\Api\Users\Order\GetDetailsRequest;
use App\Http\Requests\Api\Users\Order\RateRequest;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function add(AddRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $response = $this->orderService->addOrder($validated, $user);
        return $this->returnSuccessMessage($response);
    }

    public function getPending()
    {
        $user = auth()->user();
        $data = $this->orderService->getPendingOrders($user);
        return $this->returnData('data', $data);
    }

    public function getArchived()
    {
        $user = auth()->user();
        $data = $this->orderService->getArchivedOrders($user);
        return $this->returnData('data', $data);
    }

    public function getDetails(GetDetailsRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $data = $this->orderService->getOrderDetails($validated, $user);
        return $this->returnData('data', $data);
    }

    public function delete(DeleteRequest $request)
    {
        $validated = $request->validated();
        $response = $this->orderService->deleteOrder($validated);
        return $this->returnSuccessMessage($response);
    }

    public function rate(RateRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $response = $this->orderService->rateOrder($validated, $user);
        return $this->returnSuccessMessage($response);
    }
}
