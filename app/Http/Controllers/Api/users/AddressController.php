<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Users\Address\CreateRequest;
use App\Http\Requests\Api\Users\Address\DeleteRequest;
use App\Http\Requests\Api\Users\Address\UpdateRequest;
use App\Services\AddressService;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    protected $addressService;

    public function __construct(AddressService $addressService)
    {
        $this->addressService = $addressService;
    }

    public function get()
    {
        $user = auth()->user();
        $data = $this->addressService->getAddresses($user->id);
        return $this->returnData('data', $data);
    }

    public function create(CreateRequest $request)
    {
        $validated = $request->validated();
        $user = auth()->user();
        $data = $validated->only(['name', 'city', 'street', 'phone', 'location_lat', 'location_long']);
        $this->addressService->createAddress($user->id, $data);
        return $this->returnSuccessMessage('Address Added');
    }

    public function update(UpdateRequest $request)
    {
        $validated = $request->validated();
        $data = $validated->only(['name', 'city', 'street', 'phone', 'location_lat', 'location_long']);
        $result = $this->addressService->updateAddress($validated->address_id, $data);
        return $result === 'Address Updated' ? $this->returnSuccessMessage($result) : $this->returnError($result);
    }

    public function delete(DeleteRequest $request)
    {
        $validated = $request->validated();
        $result = $this->addressService->deleteAddress($validated->address_id);
        return $result === 'Address Deleted' ? $this->returnSuccessMessage($result) : $this->returnError($result);
    }
}
