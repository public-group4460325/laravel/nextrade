<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use App\Services\NotificationService;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    protected $notificationService;

    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    public function get()
    {
        $user = auth()->user();
        $data = $this->notificationService->getUserNotifications($user);

        return $this->returnData('data', $data);
    }
}
