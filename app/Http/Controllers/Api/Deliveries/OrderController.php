<?php

namespace App\Http\Controllers\Api\Deliveries;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Deliveries\Order\ApproveRequest;
use App\Http\Requests\Api\Deliveries\Order\DeliverRequest;
use App\Http\Requests\Api\Deliveries\Order\GetDetailsRequest;
use App\Services\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function approve(ApproveRequest $request)
    {
        $validated = $request->validated();
        $result = $this->orderService->approveOrder($validated->order_id, auth()->user()->id);
        return response()->json($result);
    }

    public function deliver(DeliverRequest $request)
    {
        $validated = $request->validated();
        $result = $this->orderService->deliverOrder($validated->order_id, auth()->user()->id);
        return response()->json($result);
    }

    public function getPending()
    {
        $result = $this->orderService->getPendingOrders();
        return response()->json($result);
    }

    public function getArchived()
    {
        $result = $this->orderService->getArchivedOrders(auth()->user()->id);
        return response()->json($result);
    }

    public function getAccepted()
    {
        $result = $this->orderService->getAcceptedOrders(auth()->user()->id);
        return response()->json($result);
    }

    public function getDetails(GetDetailsRequest $request)
    {
        $validated = $request->validated();
        $result = $this->orderService->getOrderDetails($validated->order_id);
        return response()->json($result);
    }
}
