<?php

namespace App\Http\Controllers\Api\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admins\Item\CreateRequest;
use App\Http\Requests\Api\Admins\Item\DeleteRequest;
use App\Http\Requests\Api\Admins\Item\GetRequest;
use App\Http\Requests\Api\Admins\Item\searchRequest;
use App\Http\Requests\Api\Admins\Item\UpdateRequest;
use App\Services\ItemService;
use App\Traits\ApiTrait;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    use ApiTrait;

    protected $itemService;

    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    public function create(CreateRequest $request)
    {
        $validated = $request->validated();
        $path = $validated->file('image')->store('images/items', 'public');
        $data = $validated->validated();
        $data['image'] = $path;
        $this->itemService->create($data);
        return $this->returnSuccessMessage('Item Created');
    }

    public function delete(DeleteRequest $request)
    {
        $validated = $request->validated();
        $this->itemService->delete($validated->item_id);
        return $this->returnSuccessMessage('Item Deleted');
    }

    public function update(UpdateRequest $request)
    {
        $validated = $request->validated();
        $data = $validated->all();
        if ($validated->hasFile('image')) {
            $data['image'] = $validated->file('image');
        }
        $this->itemService->update($request->item_id, $data);
        return $this->returnSuccessMessage('Item Updated');
    }

    public function get(GetRequest $request)
    {
        $validated = $request->validated();
        $data = $this->itemService->get($validated->category_id);
        return $this->returnData('data', $data);
    }

    public function search(searchRequest $request)
    {
        $validated = $request->validated();
        $data = $this->itemService->search($validated->name);
        return $this->returnData('data', $data);
    }
}
