<?php

namespace App\Http\Controllers\Api\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admins\Order\ApproveRequest;
use App\Http\Requests\Api\Admins\Order\GetDetailsRequest;
use App\Http\Requests\Api\Admins\Order\PrepareRequest;
use App\Services\OrderService;
use App\Traits\ApiTrait;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use ApiTrait;

    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function getArchived()
    {
        $data = $this->orderService->getArchived();
        return $this->returnData('data', $data);
    }

    public function getPending()
    {
        $data = $this->orderService->getPending();
        return $this->returnData('data', $data);
    }

    public function getAccepted()
    {
        $data = $this->orderService->getAccepted();
        return $this->returnData('data', $data);
    }

    public function approve(ApproveRequest $request)
    {
        $validated = $request->validated();
        $message = $this->orderService->approve($validated->order_id);
        return $this->returnSuccessMessage($message);
    }

    public function prepare(PrepareRequest $request)
    {
        $validated = $request->validated();
        $message = $this->orderService->prepare($validated->order_id);
        return $this->returnSuccessMessage($message);
    }

    public function getDetails(GetDetailsRequest $request)
    {
        $validated = $request->validated();
        $data = $this->orderService->getDetails($validated->order_id);
        return $this->returnData('data', $data);
    }
}
