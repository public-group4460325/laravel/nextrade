<?php

namespace App\Http\Controllers\Api\Admins;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Admins\Category\CreateRequest;
use App\Http\Requests\Api\Admins\Category\DeleteRequest;
use App\Http\Requests\Api\Admins\Category\UpdateRequest;
use App\Services\CategoryService;
use App\Traits\ApiTrait;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use ApiTrait;

    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function create(CreateRequest $request)
    {
        $validated = $request->validated();
        $this->categoryService->create($validated);
        return $this->returnSuccessMessage('Category Created');
    }

    public function get()
    {
        $data = $this->categoryService->getAll();
        return $this->returnData('data', $data);
    }

    public function delete(DeleteRequest $request)
    {
        $validated = $request->validated();
        $this->categoryService->delete($validated);
        return $this->returnSuccessMessage('Category Deleted');
    }

    public function update(UpdateRequest $request)
    {
        $validated = $request->validated();
        $this->categoryService->update($validated);
        return $this->returnSuccessMessage('Category Updated');
    }
}
