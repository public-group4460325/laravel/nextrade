<?php

namespace App\Services;

use App\Http\Controllers\NotificationController;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Coupon;
use App\Repositories\OrderRepository;

class OrderService
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function approveOrder($orderId, $userId)
    {
        $order = $this->orderRepository->find($orderId);
        if (!$order || $order->receive_type != 'delivery') {
            return ['error' => 'Order Not Found'];
        }
        if ($order->status != 2) {
            return ['error' => 'Already Approved'];
        }

        $order->status++;
        $order->delivery_id = $userId;
        $this->orderRepository->save($order);

        $notData = ['page_name' => 'order'];
        $notTitle = 'order_approved_from_delivery';
        $notBody = "order_number { {$order->id} } on_the_deliver";
        $topic = "users{$order->user_id}";
        $this->sendNotificationToTopic($topic, $notTitle, $notBody, $notData);

        $notificationController = new NotificationController();
        $notificationController->addNotification($notTitle, $notBody, $order->user_id);
        $notTitle = 'order_approved_from_delivery';
        $notBody = "order_number { {$order->id} } approved_by_a_delivery_number { {$order->user_id} }";
        $topic = "admins";
        $this->sendNotificationToTopic($topic, $notTitle, $notBody, $notData);

        $notificationController->addNotification($notTitle, $notBody);
        $notTitle = 'order_approved_from_delivery';
        $notBody = "order_number { {$order->id} } approved_by_a_delivery_number { {$order->user_id} }";
        $topic = "deliveries";
        $this->sendNotificationToTopic($topic, $notTitle, $notBody, $notData);
        
        $notificationController = new NotificationController();
        $notificationController->addNotification($notTitle, $notBody);

        return ['message' => 'Approved'];
    }

    public function deliverOrder($orderId, $userId)
    {
        $order = $this->orderRepository->find($orderId);
        if (!$order || $order->receive_type != 'delivery') {
            return ['error' => 'Order Not Found'];
        }
        if ($order->status != 3) {
            return ['error' => 'Already Delivered'];
        }

        $order->status++;
        $this->orderRepository->save($order);

        $notData = ['page_name' => 'order'];
        $notTitle = 'order_delivered';
        $notBody = "order_number { {$order->id} } has_been_delivered_number { {$order->user_id} }";
        $topic = "users{$order->user_id}";
        $this->sendNotificationToTopic($topic, $notTitle, $notBody, $notData);

        $notBody = "order_number { {$order->id} } has_been_delivered_number { {$order->user_id} }";
        $topic = "admins";
        $this->sendNotificationToTopic($topic, $notTitle, $notBody, $notData);
        
        $notificationController = new NotificationController();
        $notificationController->addNotification($notTitle, $notBody);

        return ['message' => 'Delivered'];
    }

    public function getPendingOrders()
    {
        return $this->orderRepository->findByStatus(2);
    }

    public function getArchivedOrders($userId)
    {
        return $this->orderRepository->findByStatusAndDeliveryId(4, $userId);
    }

    public function getAcceptedOrders($userId)
    {
        return $this->orderRepository->findByStatusAndDeliveryId(3, $userId);
    }

    public function getOrderDetails($orderId)
    {
        $order = $this->orderRepository->find($orderId);
        if (!$order) {
            return ['error' => 'Order Not Found'];
        }

        $items = Cart::where('order_id', $order->id)->get();
        if (!$items) {
            return ['error' => 'No Items Found'];
        }

        $address = $order->address_id ? Address::find($order->address_id) : [];
        if ($order->address_id && !$address) {
            return ['error' => 'Address Not Found'];
        }

        $itemsData = $items->map(function ($item) use ($order) {
            $count = $item->count;
            $item = $item->items;
            $item['price'] -= ($item['price'] * $item['discount'] / 100);
            if ($order->coupon_id) {
                $coupon = Coupon::find($order->coupon_id);
                $item['price'] -= ($item['price'] * $coupon->discount / 100);
            }
            $item['count'] = $count;
            return $item;
        });

        return [
            'items' => $itemsData,
            'address' => $address
        ];
    }
}
