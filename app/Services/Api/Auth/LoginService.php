<?php

namespace App\Services\Api\Auth;

use App\Repositories\Api\Auth\LoginRepository;
use App\Traits\SendEmailTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class LoginService
{

    use SendEmailTrait;

    protected $loginRepository;

    public function __construct(LoginRepository $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }

    public function login($request)
    {
        $user = $this->loginRepository->getUserByEmail($request->email);
        if (!$user) return ['status' => 'error', 'message' => 'User Not Found'];
        if (!$user->email_verified_at) return ['status' => 'error', 'message' => 'Email Not Verified'];
        if (!Hash::check($request->password, $user->password)) return ['status' => 'error', 'message' => 'Wrong Password'];

        $user->user_type = $this->loginRepository->getUserType($user->user_type_id);
        $token = $user->createToken('apiToken', [$user->user_type])->plainTextToken;

        return ['status' => 'success', 'data' => ['user' => $user, 'token' => $token]];
    }

    public function logout()
    {
        auth()->user()->currentAccessToken()->delete();
        return ['status' => 'success', 'data' => 'Logout Success'];
    }

    public function checkEmail($request)
    {
        $user = $this->loginRepository->getUserByEmail($request->email);
        if (!$user) return ['status' => 'error', 'message' => 'User Not Found'];

        $verificationCode = $this->generateVerificationCode();
        $user->verification_code = $verificationCode;
        $this->loginRepository->saveUser($user);

        $this->sendVerificationEmail($request->email, $user->name, $verificationCode);

        return ['status' => 'success', 'data' => 'Mail Sent'];
    }

    public function resetPassword($request)
    {
        $user = $this->loginRepository->getUserByEmail($request->email);
        if (!$user) return ['status' => 'error', 'message' => 'User Not Found'];

        $user->password = $request->password;
        $this->loginRepository->saveUser($user);

        return ['status' => 'success', 'data' => 'Password Reset'];
    }

    public function verifyCodeForgetPassword($request)
    {
        $user = $this->loginRepository->getUserByEmail($request->email);
        if (!$user) return ['status' => 'error', 'message' => 'User Not Found'];
        if ($request->verification_code != $user->verification_code) return ['status' => 'error', 'message' => 'Verification Code Not Match'];

        $user->email_verified_at = Carbon::now();
        $this->loginRepository->saveUser($user);

        return ['status' => 'success', 'data' => 'Email Verified'];
    }

    private function generateVerificationCode()
    {
        $numbers = range(0, 9);
        shuffle($numbers);
        return implode(array_slice($numbers, 0, 5));
    }
}
