<?php

namespace App\Services\Api\Auth;

use App\Repositories\Api\Auth\SignUpRepository;
use App\Traits\SendEmailTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class SignUpService
{
    use SendEmailTrait;

    protected $signUpRepository;

    public function __construct(SignUpRepository $signUpRepository)
    {
        $this->signUpRepository = $signUpRepository;
    }

    public function signUp($request)
    {
        $user = $this->signUpRepository->getUserByEmail($request->email);
        if ($user) return ['status' => 'error', 'message' => 'User Was Already Exist'];

        $verificationCode = $this->generateVerificationCode();
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'verification_code' => $verificationCode,
        ];

        $this->signUpRepository->createUser($data);
        $this->sendVerificationEmail($request->email, $request->name, $verificationCode);

        return ['status' => 'success', 'message' => 'Mail Sent'];
    }

    public function verifyCodeSignUp($request)
    {
        $user = $this->signUpRepository->getUserByEmail($request->email);
        if (!$user) return ['status' => 'error', 'message' => 'User Not Found'];
        if ($request->verification_code != $user->verification_code) return ['status' => 'error', 'message' => 'Verification Code Not Match'];

        $user->email_verified_at = Carbon::now();
        $this->signUpRepository->saveUser($user);

        return ['status' => 'success', 'message' => 'Email Verified'];
    }

    public function resendVerificationCode($request)
    {
        $user = $this->signUpRepository->getUserByEmail($request->email);
        if (!$user) return ['status' => 'error', 'message' => 'User Not Found'];

        $verificationCode = $this->generateVerificationCode();
        $user->verification_code = $verificationCode;
        $this->signUpRepository->saveUser($user);

        $this->sendVerificationEmail($request->email, $user->name, $verificationCode);

        return ['status' => 'success', 'message' => 'Mail Sent'];
    }

    private function generateVerificationCode()
    {
        $numbers = range(0, 9);
        shuffle($numbers);
        return implode(array_slice($numbers, 0, 5));
    }
}
