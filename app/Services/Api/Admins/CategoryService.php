<?php

namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function create($request)
    {
        $path = $request->file('image')->store('images/categories', 'public');
        $data = [
            'name_en' => $request->name_en,
            'name_ar' => $request->name_ar,
            'image' => $path,
        ];
        return $this->categoryRepository->create($data);
    }

    public function getAll()
    {
        return $this->categoryRepository->getAll();
    }

    public function delete($request)
    {
        $category = $this->categoryRepository->find($request->category_id);
        if (!$category) {
            throw new \Exception('Category Not Found');
        }

        $this->categoryRepository->delete($category);
    }

    public function update($request)
    {
        $category = $this->categoryRepository->find($request->category_id);
        if (!$category) {
            throw new \Exception('Category Not Found');
        }

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('images/categories', 'public');
            $request->merge(['image' => $path]);
        }

        $this->categoryRepository->update($category, $request->only('name_en', 'name_ar', 'image'));
    }
}
