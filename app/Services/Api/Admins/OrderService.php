<?php

namespace App\Services;

use App\Http\Controllers\NotificationController;
use App\Models\Notification;
use App\Repositories\OrderRepository;
use App\Traits\ApiTrait;
use App\Traits\NotificationTrait;

class OrderService
{

    use ApiTrait;
    use NotificationTrait;

    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function getArchived()
    {
        return $this->orderRepository->getArchived();
    }

    public function getPending()
    {
        return $this->orderRepository->getPending();
    }

    public function getAccepted()
    {
        return $this->orderRepository->getAccepted();
    }

    public function approve($orderId)
    {
        $order = $this->orderRepository->find($orderId);
        if (!$order) throw $this->returnError('Order Not Found');
        if ($order->status != 0) return $this->returnError('Already Approved');
        
        $this->orderRepository->updateStatus($order, $order->status + 1);

        $notData = [
            'page_name' => 'order',
        ];
        $notTitle = 'order_approved';
        $notBody = "order_number { {$order->id} } has_been_approved";
        $topic = "users{$order->user_id}";
        
        $this->sendNotificationToTopic($topic, $notTitle, $notBody, $notData);
        $notificationController = new NotificationController();
        $notificationController->addNotification($notTitle, $notBody, $order->user_id);

        return 'Approved';
    }

    public function prepare($orderId)
    {
        $order = $this->orderRepository->find($orderId);
        if (!$order) $this->returnError('Order Not Found');
        if ($order->status != 1) return $this->returnError('Already Prepared');

        $newStatus = $order->receive_type == 'delivery' ? $order->status + 1 : 4;
        $this->orderRepository->updateStatus($order, $newStatus);

        $notData = ['page_name' => 'order'];
        $notTitle = 'order_prepared';
        $notBody = "order_number { {$order->id} } has_been_prepared";
        $topic = "users{$order->user_id}";
        $this->sendNotificationToTopic($topic, $notTitle, $notBody, $notData);

        $notTitle = 'alert';
        $notBody = "order_number { {$order->id} } ready_to_deliver";
        $topic = 'deliveries';
        $this->sendNotificationToTopic($topic, $notTitle, $notBody, $notData);
        $notificationController = new NotificationController();
        $notificationController->addNotification($notTitle, $notBody, $order->user_id);

        return 'Prepared';
    }

    public function getDetails($orderId)
    {
        $order = $this->orderRepository->find($orderId);
        if (!$order) return $this->returnError('Order Not Found');

        $items = $this->orderRepository->getItems($orderId);
        if ($items->isEmpty()) return $this->returnError('No Items Found');

        $address = $order->address_id ? $this->orderRepository->getAddress($order->address_id) : null;
        if ($order->address_id && !$address) return $this->returnError('Address Not Found');

        $items = $items->map(function ($item) use ($order) {
            $count = $item->count;
            $item = $item->items;
            $item['price'] -= $item['price'] * $item['discount'] / 100;
            
            if ($order->coupon_id) {
                $coupon = $this->orderRepository->findCoupon($order->coupon_id);
                $item['price'] -= $item['price'] * $coupon->discount / 100;
            }
            $item['count'] = $order->coupon_id ? $count : $item['count'];
            return $item;
        });

        return [
            'items' => $items,
            'address' => $address
        ];
    }
}
