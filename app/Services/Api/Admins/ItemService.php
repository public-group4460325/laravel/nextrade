<?php

namespace App\Services;

use App\Repositories\ItemRepository;
use Illuminate\Support\Facades\Auth;

class ItemService
{
    protected $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function create($data)
    {
        return $this->itemRepository->create($data);
    }

    public function delete($itemId)
    {
        $item = $this->itemRepository->find($itemId);
        if (!$item) throw new \Exception('Item Not Found');
        $this->itemRepository->delete($item);
    }

    public function update($itemId, $data)
    {
        $item = $this->itemRepository->find($itemId);
        if (!$item) throw new \Exception('Item Not Found');
        $this->itemRepository->update($item, $data);
    }

    public function get($categoryId)
    {
        $items = $this->itemRepository->getByCategory($categoryId);
        $userFavorites = Auth::user()->favoritesItems->pluck('id')->toArray();

        return $items->map(function ($item) use ($userFavorites) {
            $itemData = $item->toArray();
            $itemData['favorite'] = in_array($item->id, $userFavorites);
            return $itemData;
        });
    }

    public function search($name)
    {
        $items = $this->itemRepository->search($name);
        $userFavorites = Auth::user()->favoritesItems->pluck('id')->toArray();

        return $items->map(function ($item) use ($userFavorites) {
            $itemData = $item->toArray();
            $itemData['favorite'] = in_array($item->id, $userFavorites);
            return $itemData;
        });
    }
}
