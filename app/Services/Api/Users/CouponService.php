<?php

namespace App\Services;

use App\Repositories\CouponRepository;
use Carbon\Carbon;

class CouponService
{
    protected $couponRepository;

    public function __construct(CouponRepository $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }
    
    public function checkCoupon($code)
    {
        $coupon = $this->couponRepository->findByCode($code);
        if (!$coupon) return ['error' => 'Coupon Not Found'];
        if ($coupon->expired_date < Carbon::now()) return ['error' => 'Coupon Was Expired'];
        if ($coupon->count <= 0) return ['error' => 'Coupon Was Ended'];
        return ['data' => $coupon];
    }
}
