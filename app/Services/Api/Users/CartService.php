<?php

namespace App\Services;

use App\Repositories\CartRepository;

class CartService
{
    protected $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function addItem($userId, $itemId, $count)
    {
        $item = $this->cartRepository->findItem($itemId);
        if (!$item) return 'Item Not Found';

        $cartItem = $this->cartRepository->findCartItem($userId, $itemId);
        if (!$cartItem) {
            $data = [
                'user_id' => $userId,
                'item_id' => $itemId,
                'count' => $count,
                'price' => $item->price,
            ];
            $this->cartRepository->create($data);
        } else {
            $cartItem->count += $count;
            $this->cartRepository->update($cartItem, ['count' => $cartItem->count]);
        }

        return 'Added To Cart';
    }

    public function removeItem($userId, $itemId, $count)
    {
        $item = $this->cartRepository->findItem($itemId);
        if (!$item) return 'Item Not Found';

        $cartItem = $this->cartRepository->findCartItem($userId, $itemId);
        if (!$cartItem) return 'Item Not Found In Cart';

        if ($cartItem->count <= $count) {
            $this->cartRepository->delete($cartItem);
        } else {
            $cartItem->count -= $count;
            $this->cartRepository->update($cartItem, ['count' => $cartItem->count]);
        }

        return 'Removed From Cart';
    }

    public function getCartItems($userId)
    {
        $data = $this->cartRepository->getCartItems($userId);
        return $data->map(function ($item) use ($userId) {
            unset($item['pivot']);
            $item['count'] = $this->cartRepository->findCartItem($userId, $item->id)->count;
            return $item;
        });
    }

    public function updateItem($userId, $itemId, $count)
    {
        $item = $this->cartRepository->findItem($itemId);
        if (!$item) return 'Item Not Found';

        $cartItem = $this->cartRepository->findCartItem($userId, $itemId);
        if (!$cartItem) return 'Item Not Found In Cart';

        if ($count == 0) {
            $this->cartRepository->delete($cartItem);
        } else {
            $this->cartRepository->update($cartItem, ['count' => $count]);
        }

        return 'Updated';
    }

    public function deleteItem($userId, $itemId)
    {
        $item = $this->cartRepository->findItem($itemId);
        if (!$item) return 'Item Not Found';

        $cartItem = $this->cartRepository->findCartItem($userId, $itemId);
        if (!$cartItem) return 'Item Not Found In Cart';

        $this->cartRepository->delete($cartItem);
        return 'Deleted';
    }
}
