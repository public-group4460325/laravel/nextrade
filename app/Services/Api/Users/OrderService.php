<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\Item;
use App\Repositories\OrderRepository;
use Carbon\Carbon;

class OrderService
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function addOrder($request, $user)
    {
        $address = $this->orderRepository->findAddress($user->id, $request->address_id);
        if (!$address && $request->address_id) {
            return 'Address Not Found';
        }

        $coupon = $this->orderRepository->findCoupon($request->coupon_id);
        if (!$coupon && $request->coupon_id) {
            return 'Coupon Not Found';
        }

        if ($request->coupon_id && $coupon->expired_date < Carbon::now()) {
            return 'Coupon Was Expired';
        }

        if ($request->coupon_id && $coupon->count <= 0) {
            return 'Coupon Was Ended';
        }

        if ($request->coupon_id && $coupon) {
            $this->orderRepository->updateCoupon($coupon);
        }

        $price = !$request->coupon_id ? $request->price : $request->price - ($request->price * $coupon->discount / 100);

        $data = [
            'user_id' => $user->id,
            'address_id' => $request->address_id,
            'receive_type' => $request->receive_type,
            'payment_type' => $request->payment_type,
            'shipping' => $request->receive_type == 'drive_thru' ? 0 : $request->shipping,
            'price' => $price,
            'total_price' => $request->shipping + $price,
        ];

        if ($request->coupon_id) {
            $data['coupon_id'] = $coupon->id;
        }

        $order = $this->orderRepository->createOrder($data);

        Cart::whereNull('order_id')
            ->where('user_id', $user->id)
            ->update(['order_id' => $order->id]);

        $items = $this->orderRepository->getItemsByOrder($order->id);

        $items->map(function ($element) {
            $item = Item::find($element->item_id);
            $item->count -= $element->count;
            $item->save();
        });

        return 'Order Created';
    }

    public function getPendingOrders($user)
    {
        return $this->orderRepository->getPendingOrders($user->id);
    }

    public function getArchivedOrders($user)
    {
        $orders = $this->orderRepository->getArchivedOrders($user->id);
        return $orders->map(function ($order) use ($user) {
            $rate = $this->orderRepository->findRate($user->id, $order->id);
            $order['rated'] = $rate ? true : false;
            return $order;
        });
    }

    public function getOrderDetails($request, $user)
    {
        $order = $this->orderRepository->findOrder($request->order_id);
        if (!$order) {
            return 'Order Not Found';
        }

        $items = $this->orderRepository->getItemsByOrder($order->id);
        if (!$items) {
            return 'No Items Found';
        }

        $address = $order->address_id ? $this->orderRepository->findAddress($user->id, $order->address_id) : [];

        $items = $items->map(function ($element) {
            $item = $element->items;
            $item['count'] = $element['count'];
            $item['price'] = $element['price'];
            return $item;
        });

        return ['items' => $items, 'address' => $address];
    }

    public function deleteOrder($request)
    {
        $order = $this->orderRepository->findOrder($request->order_id);
        if (!$order) {
            return 'Order Not Found';
        }

        if ($order->status > 0) {
            return 'Already Approved';
        }

        $this->orderRepository->deleteOrder($order);
        return 'Deleted';
    }

    public function rateOrder($request, $user)
    {
        $order = $this->orderRepository->findOrder($request->order_id);
        if (!$order) {
            return 'Order Not Found';
        }

        $data = [
            'user_id' => $user->id,
            'rating' => $request->rate,
            'comment' => $request->comment,
            'order_id' => $order->id,
        ];

        $this->orderRepository->createRate($data);
        return 'Rate Created';
    }
}
