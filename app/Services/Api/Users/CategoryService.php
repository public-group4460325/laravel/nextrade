<?php

namespace App\Services;

use App\Repositories\CartRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\ItemRepository;
use App\Repositories\SettingRepository;

class CategoryService
{
    protected $categoryRepository;
    protected $itemRepository;
    protected $cartRepository;
    protected $settingRepository;

    public function __construct(CategoryRepository $categoryRepository, ItemRepository $itemRepository, CartRepository $cartRepository, SettingRepository $settingRepository) {
        $this->categoryRepository = $categoryRepository;
        $this->itemRepository = $itemRepository;
        $this->cartRepository = $cartRepository;
        $this->settingRepository = $settingRepository;
    }

    public function getCategories()
    {
        return $this->categoryRepository->getAll();
    }

    public function getHomeData()
    {
        $data = [];
        $data['categories'] = $this->categoryRepository->getAll();
        $data['offers_items'] = $this->itemRepository->getOffers();
        $topItems = $this->cartRepository->getTopItems();
        $data['top_items'] = $topItems->map(function ($item) {
            $count = $item->count;
            $item = $item->items;
            $item['count'] = $count;
            return $item;
        });
        $data['settings'] = $this->settingRepository->getAll();
        return $data;
    }
}
