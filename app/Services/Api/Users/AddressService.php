<?php

namespace App\Services;

use App\Repositories\AddressRepository;

class AddressService
{
    protected $addressRepository;

    public function __construct(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    public function getAddresses($userId)
    {
        return $this->addressRepository->getByUserId($userId);
    }

    public function createAddress($userId, $data)
    {
        $data['user_id'] = $userId;
        return $this->addressRepository->create($data);
    }

    public function updateAddress($addressId, $data)
    {
        $address = $this->addressRepository->find($addressId);
        if (!$address) return 'Address Not Found';
        $this->addressRepository->update($address, $data);
        return 'Address Updated';
    }

    public function deleteAddress($addressId)
    {
        $address = $this->addressRepository->find($addressId);
        if (!$address) return 'Address Not Found';
        $this->addressRepository->delete($address);
        return 'Address Deleted';
    }
}
