<?php

namespace App\Services;

use App\Models\Item;
use App\Repositories\FavoriteRepository;

class FavoriteService
{
    protected $favoriteRepository;

    public function __construct(FavoriteRepository $favoriteRepository)
    {
        $this->favoriteRepository = $favoriteRepository;
    }

    public function addOrRemove($userId, $itemId)
    {
        $item = Item::find($itemId);
        if (!$item) return ['error' => 'Item Not Found'];

        $favorite = $this->favoriteRepository->findByUserIdAndItemId($userId, $itemId);
        if (!$favorite) {
            $this->favoriteRepository->create(['user_id' => $userId, 'item_id' => $itemId]);
            return ['success' => 'Item Added To Favorites'];
        }

        $this->favoriteRepository->delete($favorite);
        return ['success' => 'Item Deleted From Favorites'];
    }

    public function getFavorites($userId)
    {
        $favorites = $this->favoriteRepository->findByUserIdAndItemId($userId, null); // Replace with actual method to get all favorites
        return $favorites->map(function ($item) {
            unset($item['pivot']);
            return $item;
        });
    }
}
