<?php

namespace App\Services;

use App\Repositories\ItemRepository;

class ItemService
{
    protected $itemRepository;

    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function getItems($categoryId, $user)
    {
        $items = $this->itemRepository->getItemsByCategory($categoryId);
        if (!$items) {
            return null;
        }

        $userFavorites = $user->favoritesItems->pluck('id')->toArray();
        return $items->map(function ($item) use ($userFavorites) {
            $itemData = $item->toArray();
            $itemData['favorite'] = in_array($item->id, $userFavorites);
            return $itemData;
        });
    }

    public function searchItems($name, $user)
    {
        $items = $this->itemRepository->searchItems($name);
        $userFavorites = $user->favoritesItems->pluck('id')->toArray();
        return $items->map(function ($item) use ($userFavorites) {
            $itemData = $item->toArray();
            $itemData['favorite'] = in_array($item->id, $userFavorites);
            return $itemData;
        });
    }
}
