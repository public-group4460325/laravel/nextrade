<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        \App\Models\Address::observe(\App\Observers\AddressObserver::class);
        \App\Models\Cart::observe(\App\Observers\CartObserver::class);
        \App\Models\Category::observe(\App\Observers\CategoryObserver::class);
        \App\Models\Coupon::observe(\App\Observers\CouponObserver::class);
        \App\Models\Favorite::observe(\App\Observers\FavoriteObserver::class);
        \App\Models\Item::observe(\App\Observers\ItemObserver::class);
        \App\Models\Notification::observe(\App\Observers\NotificationObserver::class);
        \App\Models\Order::observe(\App\Observers\OrderObserver::class);
        \App\Models\Rate::observe(\App\Observers\RateObserver::class);
        \App\Models\Setting::observe(\App\Observers\SettingObserver::class);
        \App\Models\User::observe(\App\Observers\UserObserver::class);
        \App\Models\UserType::observe(\App\Observers\UserTypeObserver::class);
    }

    public function commands($commands)
    {
        \App\Console\Commands\MakeLogObservers::class;
        \App\Console\Commands\MakeLogObserver::class;
        \App\Console\Commands\MakeControllerServiceRepository::class;
    }
}
